from django.contrib import admin
from recipe.models import Recipe, RecipeStep, Ingredient

# Register your models here.

# admin.site.register(Recipe)
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
    "title",
    "id",
    )

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "step_number",
        "instruction",
        "id",
    )
@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = (
       "id",
        "amount",
        "food_item"
    )
