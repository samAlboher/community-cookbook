from django.urls import path
from recipe.views import recipe_list, my_recipes, show_recipe, create_recipe, edit_post

urlpatterns = [
    path("", recipe_list, name="recipe_list"),
    path("myrecipes/", my_recipes,name="my_recipes"),
    path("<int:id>/",show_recipe, name="show_recipe"),
    path("create/", create_recipe, name="create_recipe"),
    path("edit/<int:id>/", edit_post, name="edit_post")

]
